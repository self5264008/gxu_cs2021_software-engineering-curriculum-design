/*
Navicat MySQL Data Transfer

Source Server         : localhost_3306
Source Server Version : 80028
Source Host           : localhost:3306
Source Database       : manager

Target Server Type    : MYSQL
Target Server Version : 80028
File Encoding         : 65001

Date: 2024-02-01 13:54:17
*/
-- 初始化并填充 admin 表
DROP TABLE IF EXISTS `admin`;
CREATE TABLE `admin` (
  `id` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `admin` (`id`, `account`, `password`, `phone`) VALUES
('0', 'test', '123456', '12345678901'),
('1', 'admin1', 'password1', '12345678901'),
('2', 'admin2', 'password2', '12345678902'),
('3', 'admin3', 'password3', '12345678903'),
('4', 'admin4', 'password4', '12345678904'),
('5', 'admin5', 'password5', '12345678905'),
('6', 'admin6', 'password6', '12345678906'),
('7', 'admin7', 'password7', '12345678907'),
('8', 'admin8', 'password8', '12345678908'),
('9', 'admin9', 'password9', '12345678909'),
('10', 'admin10', 'password10', '12345678910');

-- 初始化并填充 user 表
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` varchar(255) NOT NULL,
  `account` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `position` varchar(255) NOT NULL,
  `nickName` varchar(255) NOT NULL,
  `department` varchar(255) NOT NULL,
  `sex` char(1) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `mailbox` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `user` (`id`, `account`, `password`, `name`, `position`, `nickName`, `department`, `sex`, `phone`, `mailbox`) VALUES
('0', 'test', '123456', 'test', '教师', 'T', '电子信息工程系', 'F', '12345678911', 't@example.com'),
('1', 'user1', 'password1', '李华', '系主任', '小华', '电子信息工程系', 'M', '12345678901', 'lihua@example.com'),
('2', 'user2', 'password2', '王芳', '教师', '芳芳', '电子信息工程系', 'F', '12345678902', 'wangfang@example.com'),
('3', 'user3', 'password3', '张伟', '教务员', '伟伟', '电子信息工程系', 'M', '12345678903', 'zhangwei@example.com'),
('4', 'user4', 'password4', '赵敏', '实验室管理员', '小敏', '电子信息工程系', 'F', '12345678904', 'zhaomin@example.com'),
('5', 'user5', 'password5', '刘强', '辅导员', '小强', '电子信息工程系', 'M', '12345678905', 'liuq@example.com'),
('6', 'user6', 'password6', '孙丽', '招生专员', '小丽', '招生办公室', 'F', '12345678906', 'sunli@example.com'),
('7', 'user7', 'password7', '周杰', '行政助理', '小杰', '行政办公室', 'M', '12345678907', 'zhoujie@example.com'),
('8', 'user8', 'password8', '吴婷', '后勤管理', '小婷', '后勤管理办公室', 'F', '12345678908', 'wuting@example.com'),
('9', 'user9', 'password9', '郑凯', '学生会指导老师', '小凯', '学生会', 'M', '12345678909', 'zhengkai@example.com'),
('10', 'user10', 'password10', '何英', '图书馆管理员', '小英', '图书馆', 'F', '12345678910', 'heying@example.com');


-- 初始化并填充 position 表
DROP TABLE IF EXISTS `position`;
CREATE TABLE `position` (
  `id` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `status` varchar(50),
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `position` (`id`, `name`, `status`, `description`) VALUES
('1', '系主任', '活跃', '负责管理系部的整体工作。'),
('2', '教师', '活跃', '负责教学和科研工作。'),
('3', '教务员', '活跃', '负责处理教务事务。'),
('4', '实验室管理员', '活跃', '负责实验室的日常管理。'),
('5', '辅导员', '活跃', '负责学生的思想政治教育和日常管理。'),
('6', '招生专员', '活跃', '负责招生宣传和新生入学工作。'),
('7', '行政助理', '活跃', '负责行政事务处理。'),
('8', '后勤管理', '活跃', '负责后勤保障和管理工作。'),
('9', '学生会指导老师', '活跃', '负责学生会的全面工作。'),
('10', '图书馆管理员', '活跃', '负责图书馆的管理和服务。');

-- 初始化并填充 department 表
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `id` varchar(50) NOT NULL,
  `name` varchar(100) NOT NULL,
  `description` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `department` (`id`, `name`, `description`) VALUES
('1', '电子信息工程系', '负责电子信息工程专业的教学和管理。'),
('2', '招生办公室', '负责招生宣传和新生入学工作。'),
('3', '行政办公室', '负责处理学院的行政事务。'),
('4', '后勤管理办公室', '负责后勤保障和管理工作。'),
('5', '学生会', '负责学生会的组织和管理工作。'),
('6', '图书馆', '负责图书馆的管理和服务。');

-- 初始化并填充 messageitems 表
DROP TABLE IF EXISTS `messageitems`;
CREATE TABLE `messageitems` (
  `isUsed` int DEFAULT NULL,
  `id` varchar(255) NOT NULL,
  `sendId` varchar(255) NOT NULL,
  `receiverId` varchar(255) NOT NULL,
  `message` text NOT NULL,
  `sendTime` datetime DEFAULT NULL,
  `messageType` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `messageitems` (`isUsed`, `id`, `sendId`, `receiverId`, `message`, `sendTime`, `messageType`) VALUES
(0, '1', 'user1', 'user2', '下午3点开会讨论新课程设置', '2024-05-28 09:00:00', 1),
(1, '2', 'user3', 'user4', '实验室设备维护通知', '2024-05-28 10:00:00', 2),
(0, '3', 'user5', 'user6', '新生入学培训安排', '2024-05-28 11:00:00', 1),
(1, '4', 'user7', 'user8', '后勤管理工作会议', '2024-05-28 12:00:00', 1),
(0, '5', 'user9', 'user10', '学生会活动策划会议', '2024-05-28 13:00:00', 2),
(1, '6', 'user2', 'user3', '教学工作月度总结', '2024-05-28 14:00:00', 2),
(0, '7', 'user4', 'user5', '下午4点实验课准备工作', '2024-05-28 15:00:00', 1),
(1, '8', 'user6', 'user7', '新学期工作安排', '2024-05-28 16:00:00', 2),
(0, '9', 'user8', 'user9', '周六图书馆闭馆通知', '2024-05-28 17:00:00', 1),
(1, '10', 'user10', 'user1', '绩效评估通知', '2024-05-28 17:00:00', 1)