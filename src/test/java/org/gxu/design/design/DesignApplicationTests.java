package org.gxu.design.design;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.gxu.design.entity.FoundApply;
import org.gxu.design.entity.FoundSpecification;
import org.gxu.design.entity.LostSpecification;
import org.gxu.design.entity.User;
import org.gxu.design.mapper.*;
import org.gxu.design.util.Result;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.*;

@SpringBootTest
class DesignApplicationTests {

    @Autowired
    FoundApplyMapper foundApplyMapper;
    @Autowired
    FoundSpecificationMapper foundSpecificationMapper;
    @Autowired
    UserMapper userMapper;
    @Autowired
    LostApplyMapper lostApplyMapper;
    @Autowired
    LostSpecificationMapper lostSpecificationMapper;

    @Test
    void contextLoads() {
        List<Map<String, Object>> res = foundApplyMapper.selectMaps(null);
        res.forEach((e) -> {
            LambdaQueryWrapper<FoundSpecification> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(FoundSpecification::getId, e.get("foundSpecificationId"));
            List<Map<String, Object>> v = foundSpecificationMapper.selectMaps(queryWrapper);
            e.put("foundSpe", v.get(0));
            v.forEach((e2) -> {
                LambdaQueryWrapper<User> queryWrapper1 = new LambdaQueryWrapper<>();
                queryWrapper1.eq(User::getId, e2.get("foundUserId"));
                e.put("foundMsg", userMapper.selectMaps(queryWrapper1).get(0));
            });
        });
        System.out.println(Result.success(res));
    }

    @Test
    void SelectLost(){
        List<Map<String, Object>> res = lostApplyMapper.selectMaps(null);
        List<Map<String, Object>> result = new ArrayList<Map<String ,Object>>();
        res.forEach((e) -> {
            LambdaQueryWrapper<LostSpecification> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(LostSpecification::getId, e.get("lostSpecificationId"));
            List<Map<String, Object>> v = lostSpecificationMapper.selectMaps(queryWrapper);
            e.put("lostSpe", v.get(0));
            String losterApplyId = (String) v.get(0).get("losterId");
            v.forEach((e2) -> {
                LambdaQueryWrapper<User> queryWrapper1 = new LambdaQueryWrapper<>();
                queryWrapper1.eq(User::getId, e2.get("losterId"));
                e.put("lostMsg", userMapper.selectMaps(queryWrapper1).get(0));
            });
            if(e.get("auditState") == (Object) 1){
                if(e.get("itemState") == (Object) 0)
                    result.add(e);
            }else{
                System.out.println(e.get("id"));
                if(Objects.equals(losterApplyId, "1"))
                    result.add(e);
            }
        });
        result.forEach((e) ->{
            System.out.println(e.get("lostSpe"));
        });
    }
}
