package org.gxu.design.design;

import cn.hutool.core.util.RandomUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.gxu.design.entity.*;
import org.gxu.design.mapper.*;
import org.gxu.design.service.UserService;
import org.gxu.design.util.Result;
import org.gxu.design.controller.user.LostController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.*;

@SpringBootTest
public class DataGenerate {
    @Autowired
    public UserMapper userMapper;
    @Autowired
    public UserService userService;
    @Autowired
    public LostSpecificationMapper lostSpecificationMapper;
    @Autowired
    public LostApplyMapper lostApplyMapper;
    @Autowired
    public FoundApplyMapper foundApplyMapper;
    @Autowired
    public FoundSpecificationMapper foundSpecificationMapper;

    // 图片上传路径
    @Value("${myProject.imgUpload.path}")
    String uploadImgSrc;

    @Test
    public void generateUsers(){
        List<User> res = new ArrayList<>();
        for(int i = 0 ; i < 20;i++){
            User user = new User();
            user.setAccount(RandomUtil.randomString("zhang",10));
            user.setPassword("123456");
            user.setName("xgc");
            user.setNickname("风花雪月");
            user.setMajor("计算机");
            user.setSex("男");
            user.setPhone("11111");
            user.setMailbox("dwdad@qq");
            user.setLesson("幼稚园");
            res.add(user);

        }
        userService.saveBatch(res);
    }

    @Test
    public void selectUser(){
        User user = new User();
        user.setAccount("2007310446");
        user.setPassword("123456");
        LambdaQueryWrapper<User> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(User::getAccount, user.getAccount()).eq(User::getPassword, user.getPassword());
        List<User> res = userMapper.selectList(queryWrapper);
        System.out.println(res);
    }

    @Test
    public void selectLost(){
        LostSpecification lostSpecification1 = new LostSpecification();
        lostSpecification1.setItemName("心");

        List<Map<String, Object>> res = lostApplyMapper.selectMaps(null);
        res.forEach((e) ->{
            LambdaQueryWrapper<LostSpecification> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(LostSpecification::getId, e.get("lostSpecificationId"))
                    .eq(LostSpecification::getItemName, lostSpecification1.getItemName());
            List<Map<String, Object>> v = lostSpecificationMapper.selectMaps(queryWrapper);
            if(v.isEmpty()) return;
            e.put("lostSpe", v.get(0));
            v.forEach((e2) -> {
                LambdaQueryWrapper<User> queryWrapper1 = new LambdaQueryWrapper<>();
                queryWrapper1.eq(User::getId, e2.get("losterId"));
                e.put("lostMsg", userMapper.selectMaps(queryWrapper1).get(0));
            });
        });
        System.out.println(Result.success(res));
    }

    @Test
    public void selectLostAndApply() {
        List<Map<String, Object>> res = foundApplyMapper.selectMaps(null);
        res.forEach((e) -> {
            LambdaQueryWrapper<FoundSpecification> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(FoundSpecification::getId, e.get("foundSpecificationId"));
            List<Map<String, Object>> v = foundSpecificationMapper.selectMaps(queryWrapper);
            e.put("foundSpe", v.get(0));
            v.forEach((e2) -> {
                LambdaQueryWrapper<User> queryWrapper1 = new LambdaQueryWrapper<>();
                queryWrapper1.eq(User::getId, e2.get("founderId"));
                e.put("foundMsg", userMapper.selectMaps(queryWrapper1).get(0));
            });
        });
        System.out.println(Result.success(res));
    }

    @Test
    public void Test(){
        List<Map<String, Object>> res = lostApplyMapper.selectMaps(null);
        res.forEach((e) -> {
            LambdaQueryWrapper<LostSpecification> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(LostSpecification::getId, e.get("lostSpecificationId"));
            List<Map<String, Object>> v = lostSpecificationMapper.selectMaps(queryWrapper);
            e.put("lostSpe", v.get(0));
            v.forEach((e2) -> {
                LambdaQueryWrapper<User> queryWrapper1 = new LambdaQueryWrapper<>();
                queryWrapper1.eq(User::getId, e2.get("losterId"));
                e.put("lostMsg", userMapper.selectMaps(queryWrapper1).get(0));
            });
//            File file = new File(uploadImgSrc + v.get(0).get("picture"));
//            String ImgBase = getBaseImg(String.valueOf(file));
//            e.put("lostImg", ImgBase);
        });
        System.out.println(Result.success(res));
    }

}
