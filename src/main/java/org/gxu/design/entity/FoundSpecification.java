package org.gxu.design.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @TableName foundspecification
 */
@TableName(value ="foundspecification")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoundSpecification implements Serializable {
    /**
     * 
     */
    @TableId
    private String id;

    /**
     * 
     */
    private String itemName;

    /**
     * 
     */
    private String address;

    /**
     * 
     */
    private String description;


    private Date foundTime;

    /**
     * 
     */
    private String picture;

    /**
     * 
     */
    private String founderId;

    /**
     * 
     */
    private String itemCatalog;

    /**
     * 
     */
    private Date claimTime;

    /**
     * 
     */
    private String foundUserId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}