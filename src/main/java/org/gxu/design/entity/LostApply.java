package org.gxu.design.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName lostapply
 */
@TableName(value ="lostapply")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LostApply implements Serializable {
    /**
     * 
     */
    @TableId
    private String id;

    /**
     * 
     */
    private Date auditTime;

    /**
     * 
     */
    private Integer auditState;

    /**
     * 
     */
    private String lostSpecificationId;

    /**
     * 
     */
    private Integer itemState;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}