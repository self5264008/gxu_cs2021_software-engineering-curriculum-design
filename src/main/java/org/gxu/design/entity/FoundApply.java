package org.gxu.design.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

/**
 * 
 * @TableName foundapply
 */
@TableName(value ="foundapply")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FoundApply implements Serializable {
    /**
     * 
     */
    @TableId
    private String id;



    private Date aduitTime;

    /**
     * 
     */
    private Integer aduitState;

    /**
     * 
     */
    private String foundSpecificationId;

    /**
     * 
     */
    private Integer itemState;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}