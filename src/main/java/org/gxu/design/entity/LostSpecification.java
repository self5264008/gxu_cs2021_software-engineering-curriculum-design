package org.gxu.design.entity;


import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 
 * @TableName lostspecification
 */
@TableName(value ="lostspecification")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class LostSpecification implements Serializable {
    /**
     * 
     */
    @TableId
    private String id;

    /**
     * 
     */
    private String itemName;

    /**
     * 
     */
    private String address;

    /**
     * 
     */
    private Date lostTime;

    /**
     * 
     */
    private String description;

    /**
     * 
     */
    private String picture;

    /**
     * 
     */
    private String catalog;

    /**
     * 
     */
    private String losterId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}