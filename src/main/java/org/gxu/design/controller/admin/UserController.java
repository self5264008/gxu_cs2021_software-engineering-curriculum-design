package org.gxu.design.controller.admin;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.gxu.design.entity.Admin;
import org.gxu.design.entity.User;
import org.gxu.design.mapper.AdminMapper;
import org.gxu.design.mapper.FoundApplyMapper;
import org.gxu.design.mapper.FoundSpecificationMapper;
import org.gxu.design.mapper.UserMapper;
import org.gxu.design.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 用户信息管理
 */
@RestController(value = "user_admin")
@RequestMapping("/admin")
@CrossOrigin
@Slf4j
@Data
@Transactional
@ConfigurationProperties("myproject.database")
// TODO: 2024/4/9 测试完毕
public class UserController {

    @Autowired
    private AdminMapper adminMapper;
    private String[] category;
    @Value("${myproject.imgupload.path3}")
    private String imgUploadPath;
    UserMapper userMapper;
    FoundApplyMapper foundApplyMapper;
    RedisTemplate redisTemplate;
    FoundSpecificationMapper foundSpecificationMapper;
    @Resource(name = "redisLock")
    ReentrantLock redisLock;
    @Autowired
    public UserController(UserMapper userMapper, FoundApplyMapper foundApplyMapper, RedisTemplate redisTemplate, FoundSpecificationMapper foundSpecificationMapper) {
        this.userMapper = userMapper;
        this.foundApplyMapper = foundApplyMapper;
        this.redisTemplate = redisTemplate;
        this.foundSpecificationMapper = foundSpecificationMapper;
    }

    /**
     * 管理员登录
     * @param admin
     * @return
     */
    // TODO: 2024/4/9  已测试
    @PostMapping("/login")
    public Result<?> login(@RequestBody Admin admin){
        log.info("data:{}",admin);
        if (Objects.isNull(admin)){
            return Result.error("提交的参数为空！！！");
        }
        LambdaQueryWrapper<Admin> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Admin::getAccount,admin.getAccount()).eq(Admin::getPassword,admin.getPassword());
        List<Admin> admins = adminMapper.selectList(queryWrapper);
        if (Objects.isNull(admins) || admins.isEmpty()){
            return Result.error("没有此账户!!!");
        }
        return Result.success(admins.get(0));
    }

    /**
     * 获取管理员账户的信息
     * @return
     */
    // TODO: 2024/4/9 已测试
    @RequestMapping("/getAdminMsg")
    public Result<?> getAdminMsg(){
        Admin admin = adminMapper.selectOne(null);
        if (Objects.isNull(admin)){
            return Result.error("获取信息异常");
        }
        return Result.success(admin);
    }

    /**
     * 管理员更新个人信息
     * @param admin
     * @return
     */
    // TODO: 2024/4/9 已测试
    @RequestMapping({"/updateAdminMsg","/updateAdminPwd"})
    public Result<String> updateAdminMsg(@RequestBody Admin admin){
        if (Objects.isNull(admin)){
            return Result.error("请求参数不合法!");
        }
        int row = adminMapper.updateById(admin); // 受影响行数
        if (row == 0){
            return Result.error("修改失败");
        }
        return Result.success("操作成功!!!");
    }

    /**
     * 分页查询用户信息
     * @param pageNum - 页码
     * @param pageSize - 每页大小
     * @return
     */
    // TODO: 2024/3/19 已测试
    @RequestMapping("queryAllUsers")
    public Result<Page> findUsers(@RequestParam(value = "pageNum",defaultValue = "1") int pageNum, @RequestParam(value = "pageSize",defaultValue = "5") int pageSize){
        log.info("页号{},每页大小{}",pageNum,pageSize);
        Page<User> result = new Page<>(pageNum,pageSize);
        userMapper.selectPage(result,null);
        return Result.success(result);
    }

    /**
     * 管理员头像更换
     * @param file
     * @return
     */
    // TODO: 2023/4/9 已测试 (页面得刷新才能看到修改后的头像)
    @PostMapping("/updateHeaderImg")
    public Result<String> updateHeaderImg(MultipartFile file){
        if (Objects.isNull(file) || file.isEmpty()){
            return Result.error("更新失败!!!");
        }
        try {
            File oldFile = ResourceUtils.getFile(imgUploadPath + "header.png");
            log.info("旧头像{}",oldFile);
            file.transferTo(oldFile); // 待最终的修改
        }catch (Exception e){
            return Result.error("更新失败");
        }
        return Result.success("更换成功!!!");
    }

    /**
     * 获取物品分类数据
     * @return
     */
    // TODO: 2024/4/9 已测试
    @GetMapping("/getCategory")
    public Result<?> getCategory(){
        if (Objects.isNull(category)|| category.length == 0){
            return Result.error("分类数据为空");
        }
        return Result.success(category);
    }

    /**
     * 获取申请认领的数据信息
     * @return
     */
    // TODO: 2024/4/9  已测试
    @RequestMapping("/getAllApplyFetchMsg")
    public Result<?> getAllApplyFetchMsg(){
        val redis = redisTemplate.opsForValue();
        // 获取所有用户的id
        val ids = userMapper.selectAllIds();
        // 寻物请求ids
        val applyIds = foundApplyMapper.selectAllIds();
        // 没有要审核的数据，则直接返回空数组
        if ((Objects.isNull(ids)|| ids.isEmpty()) || (Objects.isNull(applyIds)|| applyIds.isEmpty())){
            return Result.success(Collections.emptyList());
        }
        Map<String, List<Map<String,Object>>> res = new HashMap<>();
        try {
            redisLock.lock();
            applyIds.forEach((applyId)->{
                List<Map<String,Object>> one = new ArrayList<>(); // 一个applyId对应的所有请求
                val itemStateKey = applyId + ":" + "itemState";
                val value = redis.get(itemStateKey);
                // 为空或者该物品已经被认领
                if (Objects.isNull(value) || (int)value == 1){
                    return;
                }
                ids.forEach((userId)->{
                    Map<String,Object> one_one = new HashMap<>(); // 一个applyId对应的一个请求
                    val userKey = applyId + ":" + userId;
                    if (redisTemplate.hasKey(userKey)){
                        val curUser = userMapper.selectById(userId);
                        val key_value = JSONUtil.toBean(redis.get(userKey).toString(),Map.class);
                        one_one.put("user",curUser);
                        one_one.put("isPassed",key_value.get("isPassed"));
                        one_one.put("reason",key_value.get("reason"));
                        val applyEntity = foundApplyMapper.selectById(applyId);
                        one_one.put("apply",applyEntity);
                        one_one.put("specMsg",foundSpecificationMapper.selectById(applyEntity.getFoundSpecificationId()));
                        // 组装数据长度不为0则应该是返回数据
                        if (!one_one.isEmpty()){
                            one.add(one_one);
                        }
                    }
                });
                if (!one.isEmpty()){
                    res.put(applyId,one);
                }
            });
        }finally {
            redisLock.unlock(); // 释放锁
        }
        return Result.success(res);
    }
}
