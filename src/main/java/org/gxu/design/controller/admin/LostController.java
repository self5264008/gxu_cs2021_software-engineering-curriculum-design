package org.gxu.design.controller.admin;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import lombok.extern.slf4j.Slf4j;
import org.gxu.design.entity.LostApply;
import org.gxu.design.entity.LostSpecification;
import org.gxu.design.entity.User;
import org.gxu.design.mapper.LostApplyMapper;
import org.gxu.design.mapper.LostSpecificationMapper;
import org.gxu.design.mapper.UserMapper;
import org.gxu.design.util.Result;
import org.gxu.design.util.SendMsgByMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.*;

/**
 * 失物招领部分
 * @author 摆烂小队
 *
 */
@RestController
@RequestMapping("/admin")
@CrossOrigin
@Slf4j
@Transactional
// TODO: 2023/4/9 测试完毕
public class LostController {


    LostApplyMapper lostApplyMapper;

    UserMapper userMapper;

    LostSpecificationMapper lostSpecificationMapper;

    SendMsgByMail sendTool;

    ThreadPoolTaskExecutor taskExecutor;

    @Autowired
    public LostController(LostApplyMapper lostApplyMapper, UserMapper userMapper, LostSpecificationMapper lostSpecificationMapper, SendMsgByMail sendTool, ThreadPoolTaskExecutor taskExecutor) {
        this.lostApplyMapper = lostApplyMapper;
        this.userMapper = userMapper;
        this.lostSpecificationMapper = lostSpecificationMapper;
        this.sendTool = sendTool;
        this.taskExecutor = taskExecutor;
    }


    /**
     * 分页查询寻物启事信息
     * @param pageNum
     * @param pageSize
     * @return
     */
    // TODO: 2023/4/9 已测试 
    @RequestMapping("/findLostMsg")
    public Result<?> queryLostMsg(@RequestParam(value = "pageNum",defaultValue = "1")Integer pageNum,
                                                         @RequestParam(value = "pageSize",defaultValue = "2")Integer pageSize)
    {
        if (pageNum <= 0 || pageSize <= 0){
            return Result.error("参数不合法!!!");
        }
        List<Map<String,Object>> res = lostApplyMapper.selectSome((pageNum - 1) * pageSize,pageSize);
        // 分页相关信息
        Map<String,Object> page = new HashMap();
        page.put("pageNum",pageNum);
        page.put("pageSize",pageSize);
        page.put("total",lostApplyMapper.selectCount(null));
        // 类似多表联查
        res.forEach((row)->{
            LostSpecification one = lostSpecificationMapper.selectById((String) row.get("lostSpecificationId"));
            row.put("lostSpe", one);
            User lost = userMapper.selectById(one.getLosterId());
            row.put("lostMsg",lost);
            row.put("page",page);
        });
        return Result.success(res);
    }

    /**
     * 通过寻物请求
     * @param id
     * @return
     */
    // TODO: 2023/4/9 已测试（可以有改进的地方:发送的邮件内容可以优化） 
    @RequestMapping(value = "/updateLostApplyState/{id}")
    public Result<String> auditLostApplyAccept(@PathVariable("id") String id, @RequestParam(value = "mailbox") String mailbox){
        LambdaUpdateWrapper<LostApply> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(LostApply::getId,id).set(LostApply::getAuditState,1).set(LostApply::getAuditTime,new Date());
        int row = lostApplyMapper.update(null, updateWrapper);
        if (row == 0){
            return Result.error("更新失败");
        }
        taskExecutor.submit(()->{
            return sendTool.sendSimpleText(new String[]{mailbox},"寻物请求审核通过!","您的xxx寻物请求已经通过审核!");
        });
        return Result.success("更新成功！！！");
    }


    /**
     * 拒绝通过寻物请求
     * @param id  寻物信息id
     * @param mailbox 请求人邮箱
     * @param text 拒绝理由
     * @return
     */
    // TODO: 2023/4/9 已测试（有改进地方：邮件内容应该包含物品的一些信息）
    @RequestMapping(value = "/rejectAudit")
    public Result<String> auditLostApplyReject(String id, String mailbox, String text){
        if (StringUtils.hasLength(id) && StringUtils.hasLength(mailbox) && StringUtils.hasLength(text)){
            LambdaUpdateWrapper<LostApply> wrapper = new LambdaUpdateWrapper<>();
            wrapper.eq(LostApply::getId,id).set(LostApply::getAuditState,2).set(LostApply::getAuditTime,new Date());
            int row = lostApplyMapper.update(null, wrapper);
            if (row != 0){
                taskExecutor.submit(()->{sendTool.sendSimpleText(new String[]{mailbox},"拒绝审核",text);});
                return Result.success("修改成功");
            }
            return Result.success("修改失败");
        }else{
            return Result.error("请求参数不合法!");
        }

    }

    /**
     * 操作说明: apply与specification是一对一关系
     * @param id
     * @return
     */
    // TODO: 2023/4/9 已测试
    @RequestMapping(value = "/deleteLostApply",method = RequestMethod.DELETE)
    public Result<String> deleteLostApply(@RequestParam("id") String id){
        if (!StringUtils.hasLength(id)){
            return Result.error("参数不合法！");
        }
        LostApply lostApply = lostApplyMapper.selectById(id);
        if (Objects.isNull(lostApply)) {
            return Result.error("删除失败");
        }else{
            lostApplyMapper.deleteById(id);
            lostSpecificationMapper.deleteById(lostApply.getLostSpecificationId());
            return Result.success("删除成功");
        }
    }
}
