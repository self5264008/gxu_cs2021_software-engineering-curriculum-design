package org.gxu.design.controller.admin
import cn.hutool.core.io.IoUtil
import cn.hutool.json.JSONUtil
import org.gxu.design.mapper.AdminMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.servlet.error.ErrorController
import org.springframework.stereotype.Controller
import org.springframework.transaction.annotation.Transactional
import org.springframework.util.ResourceUtils
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.ResponseBody
import java.io.FileInputStream
import java.nio.charset.StandardCharsets

@Controller
@RequestMapping
@Transactional
open class KotlinControllerTest {

    @Autowired
    lateinit var adminMapper:AdminMapper

    @RequestMapping(value = ["","/"])
    fun html():String{
        return "index";
    }

    @RequestMapping(value = ["/user"])
    fun userHtml():String{
        return "user/index"
    }

    @RequestMapping(value = ["/api/menus"])
    @ResponseBody
    fun menu():Map<*,*>{
        val file = ResourceUtils.getFile(ResourceUtils.getURL("classpath:menu.json"))
        val read = IoUtil.read(FileInputStream(file))
        val message = read.toString(StandardCharsets.UTF_8)
        val map = JSONUtil.toBean(message, Map::class.java)

        return map;
    }
}