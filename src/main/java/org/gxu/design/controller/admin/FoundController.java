package org.gxu.design.controller.admin;

import cn.hutool.core.util.RandomUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.gxu.design.entity.FoundApply;
import org.gxu.design.entity.FoundSpecification;
import org.gxu.design.entity.User;
import org.gxu.design.mapper.AdminMapper;
import org.gxu.design.mapper.FoundApplyMapper;
import org.gxu.design.mapper.FoundSpecificationMapper;
import org.gxu.design.mapper.UserMapper;
import org.gxu.design.util.Result;
import org.gxu.design.util.SendMsgByMail;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.TransactionStatus;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.thymeleaf.context.Context;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.ReentrantLock;

@Slf4j
@RequestMapping("/admin")
@RestController
@CrossOrigin
// TODO: 2023/4/9 测试完毕
public class FoundController {
    FoundApplyMapper foundApplyMapper;
    FoundSpecificationMapper foundspecificationMapper;
    AdminMapper adminMapper;
    UserMapper userMapper;
    SendMsgByMail sendTool;
    ThreadPoolTaskExecutor taskExecutor;
    DataSourceTransactionManager transactionManager;
    DefaultTransactionDefinition definition;
    RedisTemplate redisTemplate;
    @Resource(name = "redisLock")
    ReentrantLock redisLock;
    // 图片上传路径
    @Value("${myproject.imgupload.path2}")
    String uploadImgSrc;

    @Autowired
    public FoundController(FoundApplyMapper foundApplyMapper, FoundSpecificationMapper foundspecificationMapper, AdminMapper adminMapper, UserMapper userMapper, SendMsgByMail sendTool, ThreadPoolTaskExecutor taskExecutor, DataSourceTransactionManager transactionManager, DefaultTransactionDefinition definition,RedisTemplate redisTemplate) {
        this.foundApplyMapper = foundApplyMapper;
        this.foundspecificationMapper = foundspecificationMapper;
        this.adminMapper = adminMapper;
        this.userMapper = userMapper;
        this.sendTool = sendTool;
        this.taskExecutor = taskExecutor;
        this.transactionManager = transactionManager;
        this.definition = definition;
        this.redisTemplate = redisTemplate;
    }


    /**
     * 添加招领信息
     */
    // TODO: 2023/4/9 已经测试 
    @RequestMapping("/addFoundSpe")
    public Result<String> enterFoundInfo(FoundSpecification foundSpecification, @RequestParam(value = "upload",required = false) MultipartFile upload){
        // 未提交信息或图片上传路径加载错误
        if (Objects.isNull(uploadImgSrc) || Objects.isNull(foundSpecification)){
            return Result.error("服务器异常!");
        }
        String realDirPath = uploadImgSrc.substring(5);
        File file = new File(realDirPath);
        boolean isHaveDir = true;
        if (!file.exists()){
           try {
               file.mkdirs();
           }catch (Exception e){
               isHaveDir = false;
           }
        }
        // 拥有了存放图片的文件夹
        if (isHaveDir){
            // 手动事务
            definition.setPropagationBehavior(TransactionDefinition.PROPAGATION_REQUIRED);
            definition.setIsolationLevel(TransactionDefinition.ISOLATION_DEFAULT);
            TransactionStatus status = transactionManager.getTransaction(definition);
            try {
                // 上传数据有图片
                if (!Objects.isNull(upload)){
                    String oldFileName = upload.getOriginalFilename();
                    String suffix = oldFileName.substring(oldFileName.lastIndexOf("."));
                    String fileName = UUID.randomUUID() + suffix; // 存储的文件名
                    // 保存图片
                    upload.transferTo(new File(realDirPath + "/" + fileName));
                    foundSpecification.setPicture(fileName);

                }
                // 没有传拾物者id则默认为管理员
                if (foundSpecification.getFoundUserId() == null || foundSpecification.getFoundUserId().equals("")){
                    foundSpecification.setFoundUserId("admin");
                }
                int insert = foundspecificationMapper.insert(foundSpecification);
                // 插入成功则构建对应的foundApply数据
                if (insert != 0){
                    FoundApply apply = new FoundApply();
                    apply.setFoundSpecificationId(foundSpecification.getId());
                    apply.setAduitState(1);
                    apply.setAduitTime(new Date());
                    apply.setItemState(0);
                    int row = foundApplyMapper.insert(apply);

                }
                transactionManager.commit(status); // 事务提交
                return Result.success("添加成功!!!!");
            }catch (Exception e){
               try {
                   transactionManager.rollback(status); // 异常则回滚
               }catch (Exception e1){
                   e.printStackTrace();
               }
                return Result.error("添加失败!!!");
            }
        }
        else{
            return Result.error("添加失败!!!");
        }
    }

    /**
     * 通过审核
     * @param id
     * @param mailbox 申请人邮箱
     */
    // TODO: 2023/4/9 已测试（邮件内容方面可以改进）
    @RequestMapping("/updateAuditState")
    @Transactional
    public Result<String> auditFoundApplyAccept(String id, String mailbox){

        if (StringUtils.hasLength(id) && StringUtils.hasLength(mailbox)){
            LambdaUpdateWrapper<FoundApply> updateWrapper = new LambdaUpdateWrapper<>();
            updateWrapper.eq(FoundApply::getId,id).set(FoundApply::getAduitState,1).set(FoundApply::getAduitTime,new Date());
            int row = foundApplyMapper.update(null, updateWrapper);
            if (row == 0){
                return Result.error("更新失败");
            }
            taskExecutor.submit(()->{
                sendTool.sendSimpleText(new String[]{mailbox},"招领请求审核","xxx招领请求审核通过");
            });
            return Result.success("更新成功！！！");
        }else{
            return Result.error("请求参数不合法!");
        }
    }

    /**
     * 拒绝招领请求通过
     */
    // TODO: 2023/3/19 (邮件内容可改进)
    @RequestMapping("/rejectAuditFoundSpe")
    @Transactional
    public Result<String> auditFoundApplyReject(String id, @RequestParam(value = "mailbox",required = false)String mailbox, String text){
        LambdaUpdateWrapper<FoundApply> wrapper = new LambdaUpdateWrapper<>();
        wrapper.eq(FoundApply::getId,id).set(FoundApply::getAduitState,2).set(FoundApply::getAduitTime,new Date());
        int row = foundApplyMapper.update(null, wrapper);
        // 修改成功
        if (row != 0){
            // 提交发邮箱的任务
            taskExecutor.submit(()->{ sendTool.sendSimpleText(new String[]{mailbox}, "招领请求被驳回", text);});
            return Result.success("更新成功");
        }
        return Result.error("更新失败");

    }

    /**
     * 根据认领码标记物品被认领
     * @param id 招领请求id
     * @param code 认领码
     * @param account 用户账户- 唯一
     * 待写
     */
    // TODO: 2023/4/9 已测试
    @RequestMapping("/markClaim")
    public Result<String> signClaimed(@RequestParam(value = "id",required = false) String id,
                                      @RequestParam(value = "code",required = false)String code,
                                      @RequestParam(value = "account",required = false)String account)
    {
        if (StringUtils.hasLength(id) && StringUtils.hasLength(code) && StringUtils.hasLength(account)){
            // 认领用户信息
            val user = userMapper.selectOne(new LambdaUpdateWrapper<User>().eq(User::getAccount, account));
            if (Objects.isNull(user)){
                return Result.error("非法认领人!");
            }
            val redis = redisTemplate.opsForValue();
            val userKey = id + ":" + user.getId();
            val itemKey = id + ":" + "itemState";
            try {
                redisLock.lock();
                val itemValue = redis.get(itemKey);
                // 物品已经被认领
                if (Objects.isNull(itemValue)){
                    return Result.error("非法请求!");
                }
                if ((int)itemValue == 1){
                    return Result.error("物品已经被认领!详细信息请咨询管理员!");
                }
                // 物品未被认领
                val storeSegUserMsg = JSONUtil.toBean((String) redis.get(userKey),Map.class);
                val storeCode = storeSegUserMsg.get("code");
                // 认领码一致
                if (Objects.equals(storeCode,code)){
                    foundApplyMapper.update(null,new LambdaUpdateWrapper<FoundApply>().eq(FoundApply::getId,id).set(FoundApply::getItemState,1)); // 标记已经认领
                    redis.set(itemKey,1);
//                    redisTemplate.delete(redisTemplate.keys(id + ":*")); // 删除该apply下的所有内存数据
                    return Result.success("标记认领成功!");

                }
                else{
                    return Result.error("认领码不正确,请重新输入!");
                }
            }
            finally {
                redisLock.unlock();
            }
        }
        else{
            return Result.error("请求参数不合法!");
        }

    }

    /**
     * 审核用户认领申请
     * 同意用户的认领请求
     * @param foundApplyId
     * @param userId
     * @return
     */
    // TODO: 2023/4/9 已测试
    @RequestMapping("/acceptFetch")
    public Result<String> auditClaimAccept(String foundApplyId, String userId){
        if (StringUtils.hasLength(foundApplyId) && StringUtils.hasLength(userId)){
            val redis = redisTemplate.opsForValue();
            val userKey = foundApplyId + ":" + userId;
            // reset userKey_value
            try {
                redisLock.lock();
                if (redisTemplate.hasKey(userKey)){
                    Map<String,Object> value = JSONUtil.toBean((String)redis.get(userKey),Map.class);
                    value.put("isPassed",1); // 标记审核通过
                    val code = RandomUtil.randomNumbers(10);
                    value.put("code", code); // 认领码
                    redis.set(userKey, JSONUtil.toJsonStr(value),7, TimeUnit.DAYS); // 更新数据  认领码七天有效
                    // 发送认领码给用户
                    val foundApply = foundApplyMapper.selectById(foundApplyId);
                    val specification = foundspecificationMapper.selectById(foundApply.getFoundSpecificationId());
                    val user = userMapper.selectById(userId);
                    taskExecutor.submit(()->{
                        val context = new Context(); // 邮件数据
                        context.setVariable("user",user);
                        context.setVariable("apply",foundApply);
                        context.setVariable("specification",specification);
                        context.setVariable("code",code);
                        sendTool.sendByHTML(new String[]{user.getMailbox()},"认领码","fetchCode.html",context);
                    });
                    // 控制台输出 --- 备用方案
                    log.info("用户{}收到的认领物品{}的认领码是{}",user,specification,code);
                    return Result.success("审核通过!!!");
                }
                else{
                    return Result.error("非法请求!");
                }
            }finally {
                redisLock.unlock();
            }
        }
        else{
            return Result.error("请求参数不合法!!!");
        }
    }

    /**
     * 审核用户认领申请
     * 拒绝用户的认领请求
     * @param foundApplyId
     * @param userId
     * @param mailbox
     * @param reason
     * @return
     */
    // TODO: 2023/4/9 已测试
    @RequestMapping("/rejectFetchApply")
    public Result<?> auditClaimReject(String foundApplyId, String userId, String mailbox, String reason){
        if (StringUtils.hasLength(foundApplyId) && StringUtils.hasLength(userId)){
            // reason空白则设置默认值
            if (Objects.isNull(reason) || reason.trim().isEmpty()){
                reason = "请检查申请理由是否完善!";
            }
            val userKey = foundApplyId + ":" + userId;
            val itemKey = foundApplyId + ":" + "itemState";
            try {
                redisLock.lock();
                val redis = redisTemplate.opsForValue();
                if (redisTemplate.hasKey(userKey) && redisTemplate.hasKey(itemKey)){
                    // true说明物品已经被领走
                    if ((int)redis.get(itemKey) == 1){
                        // 邮箱信息为空
                        if (Objects.isNull(mailbox) || mailbox.trim().isEmpty()){
                            log.info("用户{}的认领申请被拒绝!",userId); // 备用方案
                        }else{
                            taskExecutor.submit(()->{sendTool.sendSimpleText(new String[]{mailbox},"认领请求","物品已经被认领，详细信息请咨询管理员!");});
                        }
                    }
                    else{
                        // 用户旧数据
                        Map<String,Object> userMsg = JSONUtil.toBean((String)redis.get(userKey),Map.class);
                        userMsg.put("isPassed",2); // 标记拒绝通过
                        // reset user data
                        redis.set(userKey,JSONUtil.toJsonStr(userMsg));
                        final String r = reason.intern();
                        if (Objects.isNull(mailbox) || mailbox.trim().isEmpty()){
                            log.info("用户{}的认领申请被拒绝!",userId); // 备用方案
                        }else{
                            taskExecutor.submit(()->{sendTool.sendSimpleText(new String[]{mailbox},"认领请求",r);});
                        }
                    }
                    return Result.success("操作成功!");
                }else{
                    return Result.error("非法请求!");
                }
            }finally {
                redisLock.unlock(); // 解锁
            }

        }else{
            return Result.error("非法的请求参数!");
        }
    }

    /**
     * 获取所有管理员和用户的信息
     */
    // TODO: 2023/4/9 已测试
    @RequestMapping("/getAllPersons")
    @Transactional
    public Result<?> getAllPersons(@RequestParam(value = "id",required = false) String id){
        List<Object> res = new ArrayList();
        if (Objects.isNull(id)){
//            res.add(adminMapper.selectList(null));
            res.add(userMapper.selectList(null));
        }
        else{
            if (id.equals("admin")){
                res.add(userMapper.selectList(null));
            }else{
                res.add(adminMapper.selectList(null));
                val wrapper = new LambdaUpdateWrapper<User>();
                wrapper.ne(User::getId,id);
                res.add(userMapper.selectList(wrapper));
            }
        }
        return Result.success(res);
    }

    /**
     * 分页查询招领信息
     */
    // TODO: 2023/4/9 已测试
    @RequestMapping("/getSpecMsg")
    @Transactional
    public Result<?> getSpecMsg(@RequestParam(value = "pageNum",defaultValue = "1") Integer pageNum,
                                @RequestParam(value = "pageSize",defaultValue = "2") Integer pageSize)
    {
        if (pageNum <= 0 || pageSize <= 0){
            return Result.error("参数不合法!!!");
        }
        // 参数合法
        final List<Map<String, Object>> res = foundApplyMapper.selectJoinByFoundSpecId((pageNum - 1) * pageSize,pageSize);
        // 分页相关信息
        final Map<String,Number> pageInfo = new HashMap<>();
        pageInfo.put("pageNum",pageNum);
        pageInfo.put("pageSize",pageSize);
        res.forEach((row)->{
            // 只执行一次
            if (pageInfo.size() != 3) {
                pageInfo.put("total", foundApplyMapper.selectCount(null)); // 总数
            }
            row.put("page",pageInfo);
            String founderId = (String) row.get("foundUserId");
            // 解决null
            founderId = founderId == null?"":founderId;
            // 招领者为管理员
            if(Objects.equals(founderId,"admin")){
                List<Map<String, Object>> admins = adminMapper.selectMaps(null);
                row.put("founder",admins.get(0));
            }
            // 招领者为普通用户
            else if (!founderId.equals("")){
                User user = userMapper.selectById(founderId);
                row.put("founder",user);

            }
        });
        return Result.success(res);
    }
}
