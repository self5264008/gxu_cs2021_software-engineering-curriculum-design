package org.gxu.design.controller.admin

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper
import org.gxu.design.entity.MessageItems
import org.gxu.design.mapper.AdminMapper
import org.gxu.design.mapper.MessageItemsMapper
import org.gxu.design.mapper.UserMapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*
@Service
@Transactional
open class ChatController {
    @Autowired
    lateinit var messageItemsMapper: MessageItemsMapper
    @Autowired
    lateinit var userMapper: UserMapper
    @Autowired
    lateinit var adminMapper: AdminMapper

    /**
     * 查询消息
     * @param senderId 消息发送者id
     * @param receiverId 消息接收者id
     */
    // TODO: 2024/4/9 已测试
    open fun viewMsg(senderId:String?,receiverId:String?):List<Map<String,Any>>{
        val queryWrapper = QueryWrapper<MessageItems>()
        queryWrapper.eq(senderId !== null,"sendId", senderId)
        queryWrapper.eq(receiverId !== null, "receiverId", receiverId)
        val res = messageItemsMapper.selectMaps(queryWrapper)
        // fill msg
        res.forEach {
            val sendId = it["sendId"] as String
            val receiveId = it["receiverId"] as String
            when(sendId){
                "admin"->{
                    it["senderMsg"] = adminMapper.selectById(sendId)
                }
                else->{
                    it["senderMsg"] = userMapper.selectById(sendId)
                }
            }
            when(receiveId){
                "admin"->{
                    it["receiverMsg"] = adminMapper.selectById(receiveId)
                }
                else ->{
                    it["receiverMsg"] = userMapper.selectById(receiveId)
                }
            }
        }
        return res
    }


    /**
     * 发送消息
     * @param msg 消息实体
     * @param senderId 发送者
     * @param receiverId 接收者
     */
    // TODO: 2024/4/9 已测试
    open fun sendMsg(msg:String, senderId: String, receiverId: String?):MessageItems
    {
        var message: MessageItems = MessageItems.build(null,0,senderId,receiverId,msg, Date())
        // 存储消息
        messageItemsMapper.insert(message)
        return message

    }

    /**
     * 撤回消息
     * @param msgId 消息ID
     */
    // TODO: 2024/4/9 已测试
    open fun recallMsg(msgId:String){
        messageItemsMapper.deleteById(msgId)
    }
}