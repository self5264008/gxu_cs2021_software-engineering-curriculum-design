package org.gxu.design.controller.user;

import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.gxu.design.entity.*;
import org.gxu.design.mapper.*;
import org.gxu.design.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.regex.Pattern;

@Slf4j
@RestController("userFoundController")
@RequestMapping("/user")
@CrossOrigin
@Transactional
public class FoundController {
    FoundSpecificationMapper foundSpecificationMapper;

    FoundApplyMapper foundApplyMapper;

    UserMapper userMapper;

    RedisTemplate redisTemplate;
    @Resource(name = "redisLock")
    ReentrantLock redisLock;

    // 图片上传路径
    @Value("${myproject.imgupload.path2}")
    String uploadImgSrc;

    @Autowired
    public FoundController(FoundSpecificationMapper foundSpecificationMapper, FoundApplyMapper foundApplyMapper, UserMapper userMapper, RedisTemplate redisTemplate) {
        this.foundSpecificationMapper = foundSpecificationMapper;
        this.foundApplyMapper = foundApplyMapper;
        this.userMapper = userMapper;
        this.redisTemplate = redisTemplate;
    }

    /**
     * 查询全部物品
     * Found库和User库联合查询
     * */
    // TODO: 2023/4/12 测试完毕 
    @PostMapping(value = {"/foundAndApply","/foundSearch"})
    public Result<List<Map<String,Object>>> searchFoundItems(@RequestBody Map<String,String> params){
        String itemName = params.get("itemName");
        String foundUserId = params.get("foundUserApplyId");
        List<Map<String, Object>> data = foundApplyMapper.selectMaps(null); // foundApply表数据
        List<Map<String, Object>> ret = new ArrayList<>();
        data.forEach((row)->{
            val condition = new LambdaQueryWrapper<FoundSpecification>();
            condition.eq(FoundSpecification::getId, row.get("foundSpecificationId")).like(!Objects.isNull(itemName),FoundSpecification::getItemName,itemName);
            val specification = foundSpecificationMapper.selectOne(condition);
            // true 则应该是结果一部分
            if (!Objects.isNull(specification)){
                if((Objects.equals(row.get("aduitState"),1) && Objects.equals(row.get("itemState"),0)) || Objects.equals(specification.getFoundUserId(), foundUserId)){
                    val file = new File(uploadImgSrc.substring(5) + specification.getPicture());
                    String fileToBase64 = getBaseImg(String.valueOf(file));
                    // 组合数据
                    row.put("foundSpe",specification);
                    row.put("foundImg",fileToBase64);
                    row.put("foundMsg",userMapper.selectById(specification.getFoundUserId()));
                    ret.add(row);
                }
            }

        });
        return Result.success(ret);
    }


    /**
     * 添加招领消息
     * @param itemName
     * @param itemAddress
     * @param itemDescription
     * @param itemTime
     * @param itemCategory
     * @param foundUserId
     * @param upload
     * @return
     * @throws ParseException
     */
    // TODO: 2023/4/12 测试完毕
    @RequestMapping("/addFoundSpe")
    public Result<String> enterFoundInfo(String itemName, String itemAddress, String itemDescription, String itemTime,
                                         String itemCategory, String foundUserId,
                                         @RequestParam(value = "upload",required = false) MultipartFile upload) throws ParseException {

        /*日期处理*/
        SimpleDateFormat Sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(itemTime !=null || itemTime.length()!=0) {
            SimpleDateFormat format1 = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss z", Locale.US);
            //开始时间 和结束时间将中国标准时间格式化为yyyy-MM-dd HH:mm:ss
            String metBeginTemp = itemTime.split(Pattern.quote("(中国标准时间)"))[0].replace("GMT+0800", "GMT+08:00");
            itemTime=Sdf.format(format1.parse(metBeginTemp));
//            System.out.println(itemTime);
        }
        Date foundTime;
        try{
            foundTime = Sdf.parse(itemTime);
//            System.out.println(foundTime);
        }catch (ParseException e){
            foundTime = null;
        }
        FoundSpecification foundSpecification = new FoundSpecification(null,itemName,itemAddress,itemDescription,foundTime,null,null,itemCategory,null,foundUserId);
        log.info("图片上传的路径为:{}",uploadImgSrc);
        log.info("收到的信息为:{}",foundSpecification);
        log.info("图片:{}",upload);
        String realDirPath = uploadImgSrc.substring(5);
        File file = new File(realDirPath);
        boolean isHaveDir = true;
        if (!file.exists()){
            try {
                file.mkdirs();
            }catch (Exception e){
                isHaveDir = false;
            }
        }
        // 拥有了存放图片的文件夹
        if (isHaveDir){
            try {
                String oldFileName = upload.getOriginalFilename();
                String suffix = oldFileName.substring(oldFileName.lastIndexOf("."));
                String fileName = UUID.randomUUID() + suffix; // 存储的文件名
                log.info(fileName);
                foundSpecification.setPicture(fileName);
                int insert = foundSpecificationMapper.insert(foundSpecification);
                // 插入成功则构建对应的foundApply数据
                if (insert != 0){
                    FoundApply apply = new FoundApply(null,null,0,foundSpecification.getId(),0);
                    int row = foundApplyMapper.insert(apply);
                    // 保存图片
                    upload.transferTo(new File(realDirPath + "/" + fileName));
                }
                return Result.success("添加成功!!!!");
            }catch (Exception e){
                return Result.error("图片上传失败!!!");
            }
        }
        else{
            return Result.error("添加失败!!!");
        }
    }

    /**
     * 用户申请认领物品
     * @param foundApplyId
     * @param userId
     * @param reason 申请原因
     */
    @PostMapping("/applyFetch")
    public Result<String> claimFoundItems(String foundApplyId, String userId, String reason){
        if (StringUtils.hasLength(foundApplyId) && StringUtils.hasLength(userId) && StringUtils.hasLength(reason)){
            try {
                val redis = redisTemplate.opsForValue();
                val itemKey = foundApplyId + ":" + "itemState";
                redisLock.lock();
                // 否, 则说明从未有过用户申请认领该物品
                if (!redisTemplate.hasKey(itemKey)){
                    redis.set(itemKey,0); // 未认领状态
                }
                // 物品已经被认领
                FoundApply temp;
                if ((int)redis.get(itemKey) == 1 || (!Objects.isNull(temp = foundApplyMapper.selectById(foundApplyId)) && temp.getItemState() == 1) ){
                    return Result.error("物品已经被认领!请联系管理员!");
                }
                // 重复申请判断
                var isCan = true;
                var isHaveCode = false;
                val userKey = foundApplyId + ":" + userId;
                if (!redisTemplate.hasKey(userKey)){
                    isCan = true;
                }else{
                    val curUserData = JSONUtil.toBean((String) redis.get(userKey),Map.class);
                    val curPassStatus = (int)curUserData.get("isPassed");
                    // 被拒绝了
                    if (curPassStatus == 2){
                        isCan = true;
                    }else if (curPassStatus == 1){
                        isCan = false;
                        isHaveCode = true;
                    }else{
                        isCan = false;
                    }
                }
                // 能申请
                if (isCan){
                    val userData = new HashMap<String,Object>();
                    userData.put("code",null);
                    userData.put("reason",reason); // 申请原因
                    userData.put("isPassed",0); // 审核是否已经通过
                    redis.set(userKey, JSONUtil.toJsonStr(userData));
                    return Result.success("请求成功!待审核");
                }else{
                    if (isHaveCode){
                        return Result.error("您已经有相应的认领码了!，请在有效期内认领!");
                    }else{
                        return Result.error("您已经申请认领过了!请等待审核!");
                    }
                }
            }catch (Exception e){
                return Result.error("申请失败!");
            }finally {
                redisLock.unlock();
            }
        }else{
            return Result.error("请求参数不合法!");
        }
    }

    /*
     * 用户申请认领按钮
     * 是否禁用认领按钮
     * */
    @PostMapping("/applyFetchButton")
    public Result<?> applyFetchButton(String foundApplyId,String userId){
        System.out.println(foundApplyId + " " + userId);
        if (StringUtils.hasLength(foundApplyId) && StringUtils.hasLength(userId)){
            try{
                val redis = redisTemplate.opsForValue();
                val itemKey = foundApplyId + ":" + "itemState";
                redisLock.lock();
                val userKey = foundApplyId + ":" + userId;
                val userData = redis.get(userKey);
                if(userData == null){
                    return Result.success("该用户未申请认领此物品");
                }else {
                    int userDataLength = userData.toString().length();
                    char userDataChar = userData.toString().charAt(userDataLength-2);
                    if(userDataChar == '2'){
                        return Result.success("用户已申请认领,但未通过!");
                    }else if(userDataChar == '1'){
                        return Result.success("用户的认领申请已通过!");
                    }else{
                        return Result.success("用户的认领申请还未通过!");
                    }
                }
            }catch (Exception e){
                System.out.println("禁用按钮功能出错！");
                return Result.error("");
            }finally {
                redisLock.unlock();
            }
        }else{
            return Result.error("请求参数不合法!");
        }
    }

    /**
     * 删除招领申请 apply 一对一于 spe
     */
    // TODO: 2023/4/12 已测试
    @PostMapping("/foundApplyDel")
    @Transactional
    public Result<?> delFoundApply(String foundApplyId){
        if (Objects.isNull(foundApplyId)){
            return Result.error("请求参数不合法");
        }
        val waitToDelApply = foundApplyMapper.selectById(foundApplyId);
        val waitToDelSpe = foundSpecificationMapper.selectById(waitToDelApply.getFoundSpecificationId());
        if (Objects.isNull(waitToDelApply) || Objects.isNull(waitToDelSpe)){
            return Result.error("要删除的信息不存在！");
        }else{
            val line1 = foundApplyMapper.deleteById(foundApplyId);
            val line2 = foundSpecificationMapper.deleteById(waitToDelApply.getFoundSpecificationId());
            if (line1 != 0 && line2 != 0){
                // 删除对应的图片
                val fileName = waitToDelSpe.getPicture();
                if (!Objects.isNull(fileName)){
                    val path = uploadImgSrc.substring(5) + fileName;
                    File file = new File(path);
                    if (!Objects.isNull(file)){
                        file.delete();
                    }
                }

                return Result.success("删除成功!");
            }else{
                return Result.error("删除失败!");
            }
        }

    }

    /**
     * 将图片base64转码
     * */
    private String getBaseImg(String imgPath){
        InputStream in;
        byte[] data = null;
        try {
            in = new FileInputStream(imgPath);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        }catch (IOException e){
            return null;
        }

        //  进行Base64编码
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(data);
    }

}