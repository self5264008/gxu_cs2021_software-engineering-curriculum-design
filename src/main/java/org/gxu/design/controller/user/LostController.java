package org.gxu.design.controller.user;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.gxu.design.entity.*;
import org.gxu.design.mapper.LostApplyMapper;
import org.gxu.design.mapper.LostSpecificationMapper;
import org.gxu.design.mapper.UserMapper;
import org.gxu.design.util.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Pattern;

@Slf4j
@RestController("userLostController")
@RequestMapping("/user")
@CrossOrigin
public class LostController {

    LostSpecificationMapper lostSpecificationMapper;

    LostApplyMapper lostApplyMapper;

    UserMapper userMapper;

    // 图片上传路径
    @Value("${myproject.imgupload.path1}")
    String uploadImgSrc;

    @Autowired
    public LostController(LostSpecificationMapper lostSpecificationMapper, LostApplyMapper lostApplyMapper, UserMapper userMapper) {
        this.lostSpecificationMapper = lostSpecificationMapper;
        this.lostApplyMapper = lostApplyMapper;
        this.userMapper = userMapper;
    }

    /**
     * Lost库和User库联合查询
     * @param params
     * @return
     */
    // TODO: 2023/4/12 测试完毕
    @PostMapping({"/lostAndApply","/lostSearch"})
    public Result<List<Map<String,Object>>> searchLostItems(@RequestBody Map<String,String> params){
        String itemName = params.get("itemName");
        String losterId = params.get("losterApplyId");
        List<Map<String, Object>> data = lostApplyMapper.selectMaps(null);
        List<Map<String, Object>> ret = new ArrayList<>();
        data.forEach((row)->{
            val condition = new LambdaQueryWrapper<LostSpecification>();
            condition.eq(LostSpecification::getId,row.get("lostSpecificationId")).like(!Objects.isNull(itemName),LostSpecification::getItemName,itemName);
            val specification = lostSpecificationMapper.selectOne(condition);
            if (!Objects.isNull(specification)){
                if ((Objects.equals(row.get("auditState"),1) && Objects.equals(row.get("itemState"),0)) || (Objects.equals(specification.getLosterId(),losterId))){
                    val file = new File(uploadImgSrc.substring(5) + specification.getPicture());
                    String base64 = getBaseImg(String.valueOf(file));
                    // 组合数据
                    row.put("lostSpe",specification);
                    row.put("lostImg",base64);
                    row.put("lostMsg",userMapper.selectById(specification.getLosterId()));
                    ret.add(row);
                }
            }
        });
        return Result.success(ret);
    }

    /**
     * 添加寻物信息
     * @param itemName
     * @param address_value
     * @param description_value
     * @param time_value
     * @param category_value
     * @param losterId
     * @param upload
     * @return
     * @throws ParseException
     */
    // TODO: 2023/4/12 已测试
    @RequestMapping("/addLostSpe")
    public Result<String> enterLostInfo(String itemName, String address_value, String description_value, String time_value,
                                         String category_value, String losterId,
                                         @RequestParam(value = "upload",required = false) MultipartFile upload) throws ParseException {

        /*日期处理*/
        SimpleDateFormat Sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        if(time_value !=null || time_value.length()!=0) {
            SimpleDateFormat format1 = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss z", Locale.US);
            //开始时间 和结束时间将中国标准时间格式化为yyyy-MM-dd HH:mm:ss
            String metBeginTemp = time_value.split(Pattern.quote("(中国标准时间)"))[0].replace("GMT+0800", "GMT+08:00");
            time_value=Sdf.format(format1.parse(metBeginTemp));
            System.out.println(time_value);
        }
        Date lostTime;
        try{
            lostTime = Sdf.parse(time_value);
            System.out.println(lostTime);
        }catch (ParseException e){
            lostTime = null;
        }
        /*添加LostSpecification类*/
        LostSpecification lostSpecification = new LostSpecification(null,itemName,address_value,lostTime,description_value,null,category_value,losterId);
        /*丢失物品图片处理*/
        log.info("图片上传的路径为:{}",uploadImgSrc);
        log.info("收到的信息为:{}",lostSpecification);
        log.info("图片:{}",upload);
        String realDirPath = uploadImgSrc.substring(5);
        File file = new File(realDirPath);
        boolean isHaveDir = true;
        if (!file.exists()){
            try {
                file.mkdirs();
            }catch (Exception e){
                isHaveDir = false;
            }
        }
        //  拥有了存放图片的文件夹
        if (isHaveDir){
            try {
                String fileName = "";
                // 图片不为null
                if(!Objects.isNull(upload)){
                    String oldFileName = upload.getOriginalFilename();
                    String suffix = oldFileName.substring(oldFileName.lastIndexOf("."));
                    fileName = UUID.randomUUID() + suffix; // 存储的文件名
                    log.info(fileName);
                    lostSpecification.setPicture(fileName);
                }
                int insert = lostSpecificationMapper.insert(lostSpecification);
                //  插入成功则构建对应的foundApply数据
                if (insert != 0){
                    LostApply apply = new LostApply(null,null,0,lostSpecification.getId(),0);
                    int row = lostApplyMapper.insert(apply);
                    // 保存图片
                    if(upload != null){
                        upload.transferTo(new File(realDirPath + "/" + fileName));
                    }
                }
                return Result.success("添加成功!!!!");
            }catch (Exception e){
                return Result.error("添加失败!!!");
            }
        }
        else{
            return Result.error("添加失败!!!");
        }
    }

    /**
     * 标记物品已找回
     */
    // TODO: 2023/4/12 已测试
    @PostMapping("/lostFounded")
    @Transactional
    public Result<?> lostFounded(String lostApplyId){
        LostApply lostApply = lostApplyMapper.selectById(lostApplyId);
        LambdaUpdateWrapper<LostApply> updateWrapper = new LambdaUpdateWrapper<>();
        updateWrapper.eq(LostApply::getId,lostApply.getId());
        updateWrapper.set(LostApply::getItemState,1);
        int updateRes = lostApplyMapper.update(lostApply, updateWrapper);
        if(updateRes == 1){
            return Result.success(lostApply);
        }
        return Result.error("找回失败!");
    }

    /**
     * 删除寻物申请
     */
    // TODO: 2023/4/12 已测试
    @PostMapping("/lostApplyDel")
    @Transactional
    public Result<String> delLostApply(String lostApplyId){
        if (Objects.isNull(lostApplyId)){
            return Result.error("请求参数不合法!");
        }else{
            val waitToDelApply = lostApplyMapper.selectById(lostApplyId);
            val waitToDelSpe = lostSpecificationMapper.selectById(waitToDelApply.getLostSpecificationId());
            if (Objects.isNull(waitToDelApply) || Objects.isNull(waitToDelSpe)){
                return Result.error("要删除的信息不存在!");
            }else{
                val line1 = lostApplyMapper.deleteById(lostApplyId);
                val line2 = lostSpecificationMapper.deleteById(waitToDelSpe.getId());
                if (line1 != 0 && line2 != 0){
                    // 删除对应图片
                    val fileName = waitToDelSpe.getPicture();
                    if (!Objects.isNull(fileName)){
                        val path = uploadImgSrc.substring(5) + fileName;
                        val file = new File(path);
                        if (!Objects.isNull(file)){
                            file.delete();
                        }
                    }
                    return Result.success("删除成功!");
                }else {
                    return Result.error("删除失败!");
                }
            }

        }
    }

    /**
     * 将图片base64转码
     * */
    private String getBaseImg(String imgPath){
        InputStream in;
        byte[] data = null;
        try {
            in = new FileInputStream(imgPath);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        }catch (IOException e){
            return null;
        }

        //  进行Base64编码
        Base64.Encoder encoder = Base64.getEncoder();
        return encoder.encodeToString(data);
    }

}
