package org.gxu.design.service;

import org.gxu.design.entity.LostApply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @description 针对表【lostapply】的数据库操作Service
* @createDate 2023-01-20 13:27:08
*/
public interface LostApplyService extends IService<LostApply> {

}
