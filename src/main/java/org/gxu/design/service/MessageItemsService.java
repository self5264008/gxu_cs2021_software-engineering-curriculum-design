package org.gxu.design.service;

import org.gxu.design.entity.MessageItems;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @description 针对表【messageitems】的数据库操作Service
* @createDate 2023-01-26 09:30:50
*/
public interface MessageItemsService extends IService<MessageItems> {

}
