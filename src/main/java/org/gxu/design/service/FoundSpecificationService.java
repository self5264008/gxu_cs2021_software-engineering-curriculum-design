package org.gxu.design.service;

import org.gxu.design.entity.FoundSpecification;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @description 针对表【foundspecification】的数据库操作Service
* @createDate 2023-01-18 19:28:11
*/
public interface FoundSpecificationService extends IService<FoundSpecification> {

}
