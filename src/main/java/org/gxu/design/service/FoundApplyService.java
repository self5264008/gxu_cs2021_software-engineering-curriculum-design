package org.gxu.design.service;

import org.gxu.design.entity.FoundApply;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @description 针对表【foundapply】的数据库操作Service
* @createDate 2023-01-18 19:27:54
*/
public interface FoundApplyService extends IService<FoundApply> {

}
