package org.gxu.design.service;

import org.gxu.design.entity.Admin;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @description 针对表【admin】的数据库操作Service
* @createDate 2023-01-18 14:12:15
*/
public interface AdminService extends IService<Admin> {

}
