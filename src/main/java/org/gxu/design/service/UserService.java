package org.gxu.design.service;

import org.gxu.design.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @description 针对表【user】的数据库操作Service
* @createDate 2023-01-18 20:18:27
*/
public interface UserService extends IService<User> {

}
