package org.gxu.design.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.gxu.design.entity.FoundApply;
import org.gxu.design.mapper.FoundApplyMapper;
import org.gxu.design.service.FoundApplyService;

import org.springframework.stereotype.Service;

/**

* @description 针对表【foundapply】的数据库操作Service实现
* @createDate 2023-01-18 19:27:54
*/
@Service
public class FoundApplyServiceImpl extends ServiceImpl<FoundApplyMapper, FoundApply>
    implements FoundApplyService {

}




