package org.gxu.design.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.gxu.design.entity.User;
import org.gxu.design.service.UserService;
import org.gxu.design.mapper.UserMapper;
import org.springframework.stereotype.Service;

/**

* @description 针对表【user】的数据库操作Service实现
* @createDate 2023-01-18 20:18:27
*/
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User>
    implements UserService{

}




