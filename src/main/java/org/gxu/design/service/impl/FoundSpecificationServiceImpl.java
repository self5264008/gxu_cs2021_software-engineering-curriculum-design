package org.gxu.design.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.gxu.design.entity.FoundSpecification;
import org.gxu.design.mapper.FoundSpecificationMapper;
import org.gxu.design.service.FoundSpecificationService;
import org.springframework.stereotype.Service;

/**

* @description 针对表【foundspecification】的数据库操作Service实现
* @createDate 2023-01-18 19:28:11
*/
@Service
public class FoundSpecificationServiceImpl extends ServiceImpl<FoundSpecificationMapper, FoundSpecification>
    implements FoundSpecificationService {

}




