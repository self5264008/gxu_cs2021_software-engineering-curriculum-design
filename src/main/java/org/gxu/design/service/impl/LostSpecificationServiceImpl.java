package org.gxu.design.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.gxu.design.entity.LostSpecification;
import org.gxu.design.service.LostSpecificationService;
import org.gxu.design.mapper.LostSpecificationMapper;
import org.springframework.stereotype.Service;

/**

* @description 针对表【lostspecification】的数据库操作Service实现
* @createDate 2023-01-20 13:28:39
*/
@Service
public class LostSpecificationServiceImpl extends ServiceImpl<LostSpecificationMapper, LostSpecification>
    implements LostSpecificationService{

}




