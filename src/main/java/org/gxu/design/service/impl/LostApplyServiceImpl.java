package org.gxu.design.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.gxu.design.entity.LostApply;
import org.gxu.design.service.LostApplyService;
import org.gxu.design.mapper.LostApplyMapper;
import org.springframework.stereotype.Service;

/**

* @description 针对表【lostapply】的数据库操作Service实现
* @createDate 2023-01-20 13:27:08
*/
@Service
public class LostApplyServiceImpl extends ServiceImpl<LostApplyMapper, LostApply>
    implements LostApplyService{

}




