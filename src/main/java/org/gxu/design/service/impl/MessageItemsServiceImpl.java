package org.gxu.design.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.gxu.design.entity.MessageItems;
import org.gxu.design.service.MessageItemsService;
import org.gxu.design.mapper.MessageItemsMapper;
import org.springframework.stereotype.Service;

/**

* @description 针对表【messageitems】的数据库操作Service实现
* @createDate 2023-01-26 09:30:50
*/
@Service
public class MessageItemsServiceImpl extends ServiceImpl<MessageItemsMapper, MessageItems>
    implements MessageItemsService{

}




