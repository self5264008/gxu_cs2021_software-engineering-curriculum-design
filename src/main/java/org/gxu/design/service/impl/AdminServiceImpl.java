package org.gxu.design.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.gxu.design.entity.Admin;
import org.gxu.design.service.AdminService;
import org.gxu.design.mapper.AdminMapper;
import org.springframework.stereotype.Service;

/**

* @description 针对表【admin】的数据库操作Service实现
* @createDate 2023-01-18 14:12:15
*/
@Service
public class AdminServiceImpl extends ServiceImpl<AdminMapper, Admin>
    implements AdminService{

}




