package org.gxu.design.service;

import org.gxu.design.entity.LostSpecification;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @description 针对表【lostspecification】的数据库操作Service
* @createDate 2023-01-20 13:28:39
*/
public interface LostSpecificationService extends IService<LostSpecification> {

}
