package org.gxu.design.socket

import io.netty.channel.Channel
import io.netty.channel.ChannelInitializer
import io.netty.channel.ChannelPipeline
import io.netty.handler.codec.http.HttpObjectAggregator
import io.netty.handler.codec.http.HttpServerCodec
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler
import io.netty.handler.codec.http.websocketx.extensions.compression.WebSocketServerCompressionHandler
import io.netty.handler.stream.ChunkedWriteHandler
import io.netty.handler.timeout.IdleStateHandler
import java.util.concurrent.TimeUnit

/**
 * 子通道初始化器
 */
class ChatServerInitializer: ChannelInitializer<Channel>() {

    /**
     * 新连接建立时，添加的编解码器
     */
    override fun initChannel(ch: Channel) {
        val pipeline: ChannelPipeline = ch.pipeline()
        pipeline.addLast(HttpServerCodec())
        pipeline.addLast(ChunkedWriteHandler())
        pipeline.addLast(HttpObjectAggregator(64 * 1024))
        pipeline.addLast(WebSocketServerCompressionHandler())
        pipeline.addLast(WebSocketServerProtocolHandler("/ws", null, true, 10 * 1024))
        pipeline.addLast(
            IdleStateHandler(
                0,
                0,
                0,
                TimeUnit.SECONDS
            )
        )
        pipeline.addLast(TextWebSocketFrameHandler())
    }
}