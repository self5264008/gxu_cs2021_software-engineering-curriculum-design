package org.gxu.design.socket

import cn.hutool.json.JSONUtil
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper
import io.netty.bootstrap.ServerBootstrap
import io.netty.channel.*
import io.netty.channel.nio.NioEventLoopGroup
import io.netty.channel.socket.nio.NioServerSocketChannel
import io.netty.handler.codec.http.websocketx.TextWebSocketFrame
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler
import lombok.extern.slf4j.Slf4j
import org.gxu.design.entity.Admin
import org.gxu.design.entity.MessageItems
import org.gxu.design.entity.User
import org.gxu.design.mapper.AdminMapper
import org.gxu.design.mapper.UserMapper
import org.springframework.stereotype.Component
import java.net.InetSocketAddress
import kotlin.collections.HashMap

@Component
class Server {
    lateinit var channel:Channel
    val group:EventLoopGroup = NioEventLoopGroup(Runtime.getRuntime().availableProcessors())

    /**
     * 启动服务
     */
    fun start(){
        // TCP 通道
        val bootstrap = ServerBootstrap()
        bootstrap.group(group)
            .channel(NioServerSocketChannel::class.java)
            .childHandler(ChatServerInitializer())
        var bind = bootstrap.bind(InetSocketAddress(9090))
        bind.syncUninterruptibly() // 等待服务启动
        channel = bind.channel()
        // JVM关闭钩子
        Runtime.getRuntime().addShutdownHook(Thread { stop() })
    }

    /**
     * 关闭服务
     */
    fun stop(){
        channel.close()
        group.shutdownGracefully()
        SessionMap.stop()
        println("netty服务器关闭!")
    }
}

/**
 * 自定义入站处理器
 */
class TextWebSocketFrameHandler : SimpleChannelInboundHandler<TextWebSocketFrame>() {
    
    override fun channelRead0(ctx: ChannelHandlerContext, msg: TextWebSocketFrame) {
        // 接收数据类型转换
        val data = JSONUtil.toBean(msg.text(), Map::class.java).toMap()
        val messageType = MessageType.valueOf(data[SEND_MSG_TYPE].toString().uppercase())
        when(messageType){ // 消息类型
            MessageType.INIT -> {
                handleInitMsg(data,ctx)
            }
            // 客户端发送信息
            MessageType.SEND -> {
                handleSendMsg(data,ctx)
            }
            // 删除信息
            MessageType.DEL ->{
                handleDelMsg(data,ctx)
            }
            // 处理已读消息
            MessageType.HADCHECK -> {
                handleHadCheckMsg(data,ctx)
            }
        }


    }

    override fun userEventTriggered(ctx: ChannelHandlerContext, evt: Any){
       when(evt){
           /**
            * 协议从http升级到websocket时的回调
            */
           is WebSocketServerProtocolHandler.HandshakeComplete -> {
               val session = Session(ctx.channel())
               SessionMap.sessionMap[session.sessionId] = session
               SessionMap.channelContextMap[session.sessionId] = ctx
               val sendMsg = HashMap<String,Any>()
               sendMsg["sessionId"] = session.sessionId
               sendMsg["type"] = "open"
               ctx.writeAndFlush(TextWebSocketFrame(JSONUtil.toJsonStr(sendMsg)))
           }

           else -> super.userEventTriggered(ctx,evt)
       }
    }

    /**
     * 底层TCP连接断开回调
     */
    override fun channelInactive(ctx: ChannelHandlerContext) {
        val session = Session.getSession(ctx)
        println("会话${session.sessionId}关闭")
        // 移除会话
        SessionMap.sessionMap.remove(session.sessionId)
        // 关闭通道
        ctx.close()
    }

    /**
     * 处理init类型的消息
     */
    private fun handleInitMsg(data: Map<Any?, Any?>, ctx: ChannelHandlerContext) {
        val curUserId = data[SENDER_USER_ID] as String
        val session = ctx.channel().attr(SESSION_KEY).get()
        // 设置session对应的用户信息
        when(curUserId){
            "admin"->{
                session.user = adminMapper!!.selectById(curUserId)
            }
            else ->{
                session.user = userMapper!!.selectById(curUserId)
            }
        }
        if(chatController !== null) {
            val msgData = chatController!!.viewMsg(curUserId,null) + chatController!!.viewMsg(null,curUserId)
            // 构建发送待发送消息
            val msg = HashMap<String,Any>()
            msg[SEND_MSG_TYPE] = "init"
            msg[SEND_MSG_KEY] = msgData
            // 发送消息
            ctx.writeAndFlush(TextWebSocketFrame(JSONUtil.toJsonStr(msg)))
        }

    }

    /**
     * 处理发送消息时的数据
     */
    private fun handleSendMsg(data: Map<Any?, Any?>, ctx: ChannelHandlerContext) {
        val senderId = data["senderId"] as String
        val receiverId = data["receiverId"] as String
        val msg = data["msg"] as String
        // store msg
        val sendMsg = chatController?.sendMsg(msg, senderId, receiverId)
        // build msg
        val res = HashMap<String,Any?>()
        res["type"] = "receive"
        res["data"] = sendMsg
        ctx.writeAndFlush(TextWebSocketFrame(JSONUtil.toJsonStr(res)))
        // 转发
        SessionMap.sessionMap.forEach{
            val value = it.value
            // 是否是消息接收者
            var isReceiver = false
            val user = value.user
            when(user){
                is User ->{
                    if (user.id == receiverId){
                        isReceiver = true
                    }
                }
                is Admin ->{
                    if (user.id == receiverId){
                        isReceiver = true
                    }
                }
            }
            if (isReceiver){
                value.channel.writeAndFlush(TextWebSocketFrame(JSONUtil.toJsonStr(res)))
            }
        }
    }

    /**
     * 处理撤回消息时的数据
     */
    private fun handleDelMsg(data: Map<Any?, Any?>, ctx: ChannelHandlerContext) {
        val msg = (data["msg"] as Map<String,Any>)
        val msgId = msg["id"] as String
        val receiverId = msg["receiverId"] as String
        // 被删除的消息
        val oldMessage = messageItemsMapper!!.selectById(msgId)
        chatController!!.recallMsg(msgId) // 删除改消息
        // 回显数据给发送者
        val sendMsg = HashMap<String,Any>()
        sendMsg["type"] = "del"
        sendMsg["data"] = oldMessage
        ctx.writeAndFlush(TextWebSocketFrame(JSONUtil.toJsonStr(sendMsg)))
        SessionMap.sessionMap.forEach{
            val session = it.value // 当前会话
            val user = session.user
            var isShouldSolve = false;
            when(user){
                is Admin ->{
                    if (user.id == receiverId){
                        isShouldSolve = true
                    }
                }
                is User->{
                    if (user.id == receiverId){
                        isShouldSolve = true
                    }
                }
            }
            if (isShouldSolve){
                session.channel.writeAndFlush(TextWebSocketFrame(JSONUtil.toJsonStr(sendMsg)))
            }
        }
    }

    /**
     * 用户已经查看消息时的回调
     */
    private fun handleHadCheckMsg(data: Map<Any?, Any?>, ctx: ChannelHandlerContext) {
        // fetch data
        val senderId = data["senderId"]
        val receiverId = data["receiverId"]
        val wrapper = UpdateWrapper<MessageItems>()
        wrapper.set("isUsed",1).eq("sendId",senderId).eq("receiverId",receiverId)
        messageItemsMapper?.update(null,wrapper)
    }
}



