package org.gxu.design.mapper;

import org.gxu.design.entity.LostSpecification;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

/**
* @author 谢广潮
* @description 针对表【lostspecification】的数据库操作Mapper
* @createDate 2023-01-20 13:28:39
* @Entity org.gxu.design.entity.LostSpecification
*/
@Repository
public interface LostSpecificationMapper extends BaseMapper<LostSpecification> {

}




