package org.gxu.design.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.gxu.design.entity.FoundSpecification;
import org.springframework.stereotype.Repository;

/**
* @author 谢广潮
* @description 针对表【foundspecification】的数据库操作Mapper
* @createDate 2023-01-18 19:28:11
* @Entity org.gxu.design.entity.Foundspecification
*/
@Repository
public interface FoundSpecificationMapper extends BaseMapper<FoundSpecification> {

}




