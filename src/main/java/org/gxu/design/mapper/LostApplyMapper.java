package org.gxu.design.mapper;

import org.apache.ibatis.annotations.Param;
import org.gxu.design.entity.LostApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* @author 谢广潮
* @description 针对表【lostapply】的数据库操作Mapper
* @createDate 2023-01-20 13:27:08
* @Entity org.gxu.design.entity.LostApply
*/
@SuppressWarnings("MybatisXMapperMethodInspection")
@Repository
public interface LostApplyMapper extends BaseMapper<LostApply> {

    List<Map<String,Object>> selectSome(@Param("offset") int offset,@Param("pageSize") int pageSize);
}




