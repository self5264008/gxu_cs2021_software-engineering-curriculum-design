package org.gxu.design.mapper;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.gxu.design.entity.FoundApply;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

/**
* @author 谢广潮
* @description 针对表【foundapply】的数据库操作Mapper
* @createDate 2023-01-18 19:27:54
* @Entity org.gxu.design.entity.Foundapply
*/
@SuppressWarnings("MybatisXMapperMethodInspection")
@Repository
public interface FoundApplyMapper extends BaseMapper<FoundApply> {
    

    List<Map<String,Object>> selectJoinByFoundSpecId(@Param("offset") int offset,@Param("pageSize")int pageSize);
    List<Map<String,Object>> selectJoinWithAdmin();
    List<String> selectAllIds();
}




